## Server API for Tic Tac Toe Game
Build with node.js, express and friends
It uses in memory emulation of DB for storing events per user session

### Local dev:
1. ```npm install``` install dependencies
2. ```npm start``` start the app
3. server should be up and running on port 3000. Example: [http://localhost:3000/events](http://localhost:3000/events)

Also app can be started using docker (docker must be installed on machine):
1. ```npm run build:docker``` builds docker image
2. ```npm run start:docker``` runs docker image (use ctrl+c to stop)
3. server should be up and running on port 3000. Example: [http://localhost:3000/events](http://localhost:3000/events)

### Available commands:
1. ```npm test``` run server tests using jest
2. ```npm start``` start the sever
3. ```npm run build:docker``` builds docker image andriuss/tic-tac-toe-api
4. ```npm run start:docker``` starts docker image andriuss/tic-tac-toe-api on port 3000
