const express = require('express');
const logger = require('morgan');
const events = require('./routes/events');
const cors = require('cors');
const session = require('express-session');

const create = (port = 3000) => {
  const app = express();

  app.use(logger('dev'));
  app.use(express.json());
  app.use(express.urlencoded({ extended: false, }));
  app.use(cors({
    credentials: true,
    origin: (origin, callback) => callback(null, true) // all origins
  }));

//app.set('trust proxy', 1);
  app.use(session({
    secret: 'tic tac toe secret key',
    resave: false,
    saveUninitialized: true,
    cookie: { secure: false, httpOnly: false }
  }));

  app.get('/', (req, res) => {
    res.json({
      'Events endpoint': `http://localhost:${port}/events`
    })
  });
  app.use('/events', events);

  const server = app.listen(port, () => console.log(`API server listening on port ${port}!`));

  return server;
}

module.exports = {
  create
};