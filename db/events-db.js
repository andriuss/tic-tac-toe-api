// Emulates DB storage or external service for storing data

let events = {};

const save = (id, event) => {
  events[id] = events[id] || [];
  events[id].push(event);
  return Promise.resolve(events[id]);
}

const loadAll = id => Promise.resolve(events[id] || []);

const removeAll = id => {
  events[id] = [];
  return Promise.resolve();
}

module.exports = {
  save,
  loadAll,
  removeAll
}