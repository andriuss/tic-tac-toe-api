const db = require('./events-db');

const anEvent = ({ player = 'Player 1', date = new Date() } = {}) => {
  return {
    player,
    date
  }
};

describe('events db', () => {
  const id  = 'id_1';
  const id2 = 'id_2';
  const event1 = anEvent({ player: 'Player 1' });
  const event2 = anEvent({ player: 'Player 2' });
  const event3 = anEvent({ player: 'Player 3' });

  beforeEach(() => db.removeAll(id));

  const givenEvents = async (events, instance = id) => {
    await Promise.all(events.map(e => db.save(instance, e)));
  };

  it('saves by id', async () => {
    const res = await db.save(id, event1);

    expect(res).toEqual([event1]);

    const events = await db.loadAll(id);
    expect(events).toEqual([event1]);
  })

  it('load by id', async () => {
    await givenEvents([event1, event2]);

    const res = await db.loadAll(id);

    expect(res).toEqual([event1, event2]);
  })

  it('load nothing', async () => {
    await givenEvents([event1, event2], id2);

    const res = await db.loadAll(id);

    expect(res).toEqual([]);
  })

  it('remove by id', async () => {
    await givenEvents([event1, event2]);
    await givenEvents([event3], id2);

    await db.removeAll(id);

    const events = await db.loadAll(id);
    expect(events).toEqual([]);
  })

  it('remove nothing', async () => {
    await givenEvents([event1], id);

    await db.removeAll(id2);

    const events = await db.loadAll(id);
    expect(events).toEqual([event1]);
  })
})