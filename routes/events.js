const router = require('express').Router();
const db = require('../db/events-db');

router.get('/', async (req, res) => {
  const events = await db.loadAll(req.sessionID);
  res.json(events);
});

router.post('/', async (req, res) => {
  const events = await db.save(req.sessionID, req.body);
  res.json(events);
});

router.delete('/', async (req, res) => {
  await db.removeAll(req.sessionID);
  res.json([]);
});

module.exports = router;
