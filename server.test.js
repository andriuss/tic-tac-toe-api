const { create } = require('axios');
const app = require('./app');

const PORT = 5000;
let server;

const startServer = async () => {
  server = await app.create(PORT);
};
const stopServer = () => server.close();

const axios = create({
  baseURL: `http://localhost:${PORT}`,
  withCredentials: true
});

beforeAll(startServer);
afterAll(stopServer);

afterEach(async () => {
  await axios.delete('/events');
});

const anEvent = ({ player ='Player 1' } = {}) => {
  return {
    player
  }
};

describe('server', () => {

  describe('GET events', () => {
    it('should return empty array', async () => {
      const res = await axios.get('/events');

      expect(res.status).toEqual(200);
      expect(res.data).toEqual([]);
    })

    it('should return array of two events', async () => {
      const event1 = anEvent({ player: 'Player 1'});
      const event2 = anEvent({ player: 'Player 2'});

      await axios.post('/events', event1);
      await axios.post('/events', event2);

      const res = await axios.get('/events');

      expect(res.data).toEqual([ event1, event2 ]);
    })
  });

  describe('POST events', () => {
    it('should add event', async () => {
      const event = anEvent();

      await axios.post('/events', event);

      const res = await axios.get('/events');
      expect(res.status).toEqual(200);
      expect(res.data).toEqual([ event ]);
    })
  });

  describe('DELETE events', () => {
    it('should delete events', async () => {
      const event1 = anEvent({ player: 'Player 1'});
      const event2 = anEvent({ player: 'Player 2'});

      await axios.post('/events', event1);
      await axios.post('/events', event2);
      await axios.delete('/events');

      const res = await axios.get('/events');
      expect(res.data).toEqual([]);
    })
  });
});