FROM node:8.12 as build-deps
WORKDIR /usr/src/app
COPY package.json ./
RUN npm install
COPY . ./
EXPOSE 3000
CMD ["npm", "start"]